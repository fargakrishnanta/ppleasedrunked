
using System;
using UnityEditor;
using UnityEngine;

[Serializable]
public class UDateTime : ISerializationCallbackReceiver
{
    public DateTime dateTime;

    [SerializeField]
    private string _dateTime;

    public static implicit operator DateTime(UDateTime udt)
    {
        return (udt.dateTime);
    }

    public static implicit operator UDateTime(DateTime dt)
    {
        return new UDateTime() { dateTime = dt };
    }

    public void OnAfterDeserialize()
    {
        DateTime.TryParse(_dateTime, out dateTime);
    }

    public void OnBeforeSerialize()
    {
        _dateTime = dateTime.ToString();
    }
}
#if UNITY_EDITOR

[CustomPropertyDrawer(typeof(UDateTime))]
public class UDateTimeDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);
        EditorGUI.PropertyField(position, property.FindPropertyRelative("_dateTime"), new GUIContent("DateTime"));
        EditorGUI.EndProperty();
    }
}

#endif