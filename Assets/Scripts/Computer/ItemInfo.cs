using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ItemInfo : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI idText, itemTypeText, locationText, dateTimeText;

    private void Awake()
    {
        ResetTextValues();
    }

    public void ParseItemData(ItemData itemData)
    {
        SetTextValues(itemData.ID, itemData.itemType.ToString(), itemData.location, itemData.dateFound.dateTime.ToString());
    }

    public void SetTextValues(string idValue, string itemTypeValue, string locationValue, string dateTimeValue)
    {
        idText.text = idValue;
        itemTypeText.text = itemTypeValue;
        locationText.text = locationValue;
        dateTimeText.text = dateTimeValue;
    }

    public void ResetTextValues()
    {
        idText.text = "";
        itemTypeText.text = "";
        locationText.text = "";
        dateTimeText.text = "";
    }
}
