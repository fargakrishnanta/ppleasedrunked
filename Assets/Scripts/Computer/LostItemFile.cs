using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class LostItemFile : MonoBehaviour, ISelectHandler
{
    public ItemInfo itemInfo;
    public ItemData itemData;

    public void OnSelect(BaseEventData baseEventData)
    {
        itemInfo.ParseItemData(itemData);
    }

    public void OnDeselect(BaseEventData baseEventData)
    {
        itemInfo.ResetTextValues();
    }
}
