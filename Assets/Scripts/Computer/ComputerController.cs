using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class ComputerController : MonoBehaviour
{
    public ItemInfo itemInfo;
    [SerializeField]
    private Canvas canvas;
    [SerializeField]
    private Button backButton;
    [SerializeField]
    private GameObject folder;
    [SerializeField]
    private GameObject folderObjectPrefab, fileObjectPrefab;

    Dictionary<ItemType, List<GameObject>> itemDictionary;
    public List<ItemData> lostItems;

    public GameController gameController;

    private void Awake()
    {
        lostItems = new List<ItemData>(Resources.LoadAll<ItemData>("LostItems"));
        itemDictionary = new Dictionary<ItemType, List<GameObject>>();
    }
    private void OnEnable()
    {
        backButton.onClick.AddListener(() => { OnBackButtonClick();
            gameController.onComputer = false;
        });
        
        Init();
    }

    private void OnDisable()
    {
        backButton.onClick.RemoveListener(OnBackButtonClick);
    }

    private void OnMouseDown()
    {
        gameController.onComputer = true;
        canvas.renderMode = RenderMode.ScreenSpaceOverlay;
    }

    private void Init()
    {
        InstantiateItemTypeFolders();

    }
    private void OnClickFolder()
    {
        
    }

    private void InstantiateItemTypeFolders()
    {
        string[] itemTypes = System.Enum.GetNames(typeof(ItemType));
        for (int i = 0; i < itemTypes.Length; i++)
        {
            GameObject newFolderObj = Instantiate(folderObjectPrefab, folder.transform);
            newFolderObj.GetComponentInChildren<TextMeshProUGUI>().text = itemTypes[i];
            ItemType type =(ItemType)System.Enum.Parse(typeof(ItemType), itemTypes[i]);
            List<ItemData> items = GetItemsOfType(type);
            List<GameObject> itemChildren = new List<GameObject>();
            for (int k = 0; k < items.Count; k++)
            {
                GameObject typeItem = Instantiate(fileObjectPrefab, folder.transform);
                typeItem.GetComponentInChildren<TextMeshProUGUI>().text = items[k].ToString();
                LostItemFile lostItemFile = typeItem.GetComponent<LostItemFile>();
                lostItemFile.itemInfo = itemInfo;
                lostItemFile.itemData = items[k];
                typeItem.SetActive(false);
                itemChildren.Add(typeItem);
            }
            newFolderObj.GetComponent<ItemTypeFolder>().itemChildren = itemChildren;
        }
    }

    public void SetItemChildrenActive(ItemType type, bool isActive)
    {
        List<GameObject> children = itemDictionary[type];
        for (int i = 0; i < children.Count; i++)
        {
            children[i].SetActive(isActive);
        } 
    }

    private List<ItemData> GetItemsOfType(ItemType itemType)
    {
        //Debug.Log("count: " + lostItems.Count);
        List<ItemData> matchingTypeItems = new List<ItemData>();
        for (int i = 0; i < lostItems.Count; i++)
        {
            //Debug.Log("lostItem: " + lostItems[i].ID + ", itemType: " + lostItems[i].itemType + " check: " + itemType);
            if (lostItems[i].itemType == itemType)
            {
                matchingTypeItems.Add(lostItems[i]);
            }
        }
        return matchingTypeItems;
    }
    private void PopulateItemTypeFolders()
    {

    }

    public void OnBackButtonClick()
    {
        canvas.renderMode = RenderMode.WorldSpace;
    }
}

