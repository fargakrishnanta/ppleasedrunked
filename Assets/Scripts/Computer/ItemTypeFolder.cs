using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemTypeFolder : MonoBehaviour 
{
    public List<GameObject> itemChildren;
    private bool isExpanded;

    void Awake()
    {
        isExpanded = false;
    }

    void OnEnable()
    {
        gameObject.GetComponent<Button>().onClick.AddListener(ToggleChildrenActive);
    }

    private void OnDisable()
    {
        gameObject.GetComponent<Button>().onClick.RemoveListener(ToggleChildrenActive);
    }

    private void ToggleChildrenActive()
    {
        isExpanded = !isExpanded;
        for (int i = 0; i < itemChildren.Count; i++)
        {
            itemChildren[i].SetActive(isExpanded);
        }
    }
}
