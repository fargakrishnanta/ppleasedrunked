using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField]
    private KeyCode _movementKey = KeyCode.LeftAlt;
    [SerializeField]
    private float _sensitivity = 2.0f;
    private float _yaw = 0.0f;
    private float _pitch = 0.0f;

    public Action onCameraMoved;
    public Action onCameraStoppedMoving;
    public bool moving = false;
    public Transform holdingTransform;

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(_movementKey))
        {
            _yaw += _sensitivity * Input.GetAxis("Mouse X");
            _pitch -= _sensitivity * Input.GetAxis("Mouse Y");
            transform.eulerAngles = new Vector3(_pitch, _yaw, 0.0f);
            moving = true;
        }
        if (Input.GetKeyDown(_movementKey))
        { 
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
        }
        if (Input.GetKeyUp(_movementKey))
        {
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
            moving = false;
        }

        if (!moving)
        {
            onCameraMoved.Invoke();
        }
        else
        {
            onCameraStoppedMoving.Invoke();
        }
    }
}
