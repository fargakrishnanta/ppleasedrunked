using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System;

public class ItemDetailPopupReturn : MonoBehaviour
{
    public TextMeshProUGUI itemID;
    public Button returnButton;
    public Button cancelButton;

    public void Init(string _itemID, Action onCancel, Action onPickup)
    {
        Reset();
        itemID.SetText(_itemID);

        returnButton.onClick.AddListener(() =>
        {
            onPickup();
        });

        cancelButton.onClick.AddListener(() =>
        {
            onCancel();
            this.gameObject.SetActive(false);
        });
    }

    public void Reset()
    {
        returnButton.onClick.RemoveAllListeners();
            cancelButton.onClick.RemoveAllListeners();
    }
}
