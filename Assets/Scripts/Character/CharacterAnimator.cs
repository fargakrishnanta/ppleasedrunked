using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class CharacterAnimator : MonoBehaviour
{
    [SerializeField] private MovementAnimation _startingMovementAnim = MovementAnimation.Standing;
    [SerializeField] private SpeakAnimation _startingSpeakAnim = SpeakAnimation.Speaking;


    private Animator _animator;

    public enum MovementAnimation
    {
        Standing,
        Drunk
    }

    public enum SpeakAnimation
    {
        Speaking,
        NotSpeaking
    }

    // Start is called before the first frame update
    void Start()
    {
        _animator = GetComponent<Animator>();

        SetMovementAnimation(_startingMovementAnim);
        SetSpeakAnimation(_startingSpeakAnim);
    }

    public void SetMovementAnimation(MovementAnimation movementAnimation)
    {
        _animator.SetTrigger(GetMovementAnimationTrigger(movementAnimation));
    }

    private string GetMovementAnimationTrigger(MovementAnimation movementAnimation)
    {
        switch(movementAnimation)
        {
            case MovementAnimation.Drunk:
                return "trigger.movement.drunk";
            case MovementAnimation.Standing:
                return "trigger.movement.standing";
        }
        return string.Empty;
    }

    public void SetSpeakAnimation(SpeakAnimation speakAnimation)
    {
        _animator.SetTrigger(GetSpeakAnimationTrigger(speakAnimation));
    }

    private string GetSpeakAnimationTrigger(SpeakAnimation speakAnimation)
    {
        switch (speakAnimation)
        {
            case SpeakAnimation.Speaking:
                return "trigger.speak.speaking";
            case SpeakAnimation.NotSpeaking:
                return "trigger.speak.not_speaking";
        }
        return string.Empty;
    }
}
