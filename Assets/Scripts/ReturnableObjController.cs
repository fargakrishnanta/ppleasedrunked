using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReturnableObjController : MonoBehaviour
{
    public ItemData itemData;

    public Vector3 originalPosition;

    private void Start()
    {
        originalPosition = transform.position;
    }
}
