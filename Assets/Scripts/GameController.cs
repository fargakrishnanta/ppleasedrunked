using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    public Button startButton;
    public GameObject startScreen;
    public GameObject goodEndScreen;
    public GameObject badEndScreen;

    public Button playAgainButton;
    public Button playAgainButton1;

    public DialogManager dialogManager;

    public Transform customerTransform;
    public Transform waitingTransform;
    public Transform servedTransform;

    public CustomerController currentCustomer;

    public List<GameObject> characters = new List<GameObject>();

    private int currentIndex = 0;

    public int currentScore;
    public int maxScore;

    public bool started = false;
    public bool onComputer = false;
    // Start is called before the first frame update
    void Start()
    {
        dialogManager = GameObject.FindObjectOfType<DialogManager>();
        dialogManager.inDialogSetCompleted = AfterCustomerServed;
        currentCustomer = customerTransform.GetComponentInChildren<CustomerController>();
        maxScore = characters.Count;
        dialogManager.currentCharacter = currentCustomer.gameObject;
        startButton.onClick.AddListener(() => {
            startScreen.SetActive(false);
            dialogManager.Initiate(currentCustomer.characterData.dialog);
            started = true;
        });
    }

    public void CheckIfReturnedItemCorrect(ReturnableObjController returnableCont, Action onReturned)
    {
        var stats = currentCustomer.characterData.itemLost.ID == returnableCont.itemData.ID;
        if (stats)
            currentScore++;

        dialogManager.ForceShowResult(stats);
        onReturned();
    }
    public void AfterCustomerServed()
    {
        currentCustomer.transform.SetParent(servedTransform);
        StartCoroutine(MoveCustomer(currentCustomer.gameObject, customerTransform, servedTransform, 1.5f, currentIndex, ()=>
        {
            characters[currentIndex].gameObject.SetActive(false);

            if (currentIndex == characters.Count - 1) {
                EndGame();
                started = false;
                return;
            }

            currentIndex++;

            StartCoroutine(MoveCustomer(characters[currentIndex], waitingTransform, customerTransform, 1.5f, currentIndex, () => {
                currentCustomer = characters[currentIndex].GetComponentInChildren<CustomerController>();
                currentCustomer.transform.SetParent(customerTransform);
                dialogManager.currentCharacter = currentCustomer.gameObject;
                dialogManager.Initiate(currentCustomer.characterData.dialog);
                started = true;
            }));
        }));

       
    }

    IEnumerator MoveCustomer(GameObject obj, Transform source, Transform target,  float overtime, int index, Action onEnd)
    {
        started = false;
        obj.GetComponent<CharacterAnimator>().SetMovementAnimation(CharacterAnimator.MovementAnimation.Drunk);
        obj.GetComponent<CharacterAnimator>().SetSpeakAnimation(CharacterAnimator.SpeakAnimation.NotSpeaking);
        float startTime = Time.time;
        while (Time.time < startTime + overtime)
        {
            obj.transform.position = Vector3.Lerp(source.position, target.position, (Time.time - startTime) / overtime);
            obj.transform.rotation = Quaternion.Lerp(source.rotation, target.rotation, (Time.time - startTime) / (overtime/2));
            yield return null;
        }
        obj.transform.position = target.position;
        obj.transform.rotation = target.rotation;
        onEnd();
        
    }

    public void EndGame()
    {
        if(currentScore == maxScore)
        {
            goodEndScreen.gameObject.SetActive(true);
            playAgainButton.onClick.AddListener(() => SceneManager.LoadScene("Main"));
        } else
        {
            badEndScreen.gameObject.SetActive(true);
            playAgainButton1.onClick.AddListener(() => SceneManager.LoadScene("Main"));
        }
    }


}
