using UnityEngine;
using TMPro;
using System;
using UnityEngine.UI;
using System.Collections.Generic;

public class DialogController : MonoBehaviour
{
    public TextMeshProUGUI dialogText;
    public Transform choiceTransform;
    public Button nextButton;
    public Button choicePrefab;
    public Color newColor;
    public Color noItemColor;
    List<QuestionAnswerData> _choices;
    List<QuestionAnswerData> _lastChoices = new List<QuestionAnswerData>();
    public Action<bool> onNoItemFound;
    public void Init(string text, Action<string> onClickCallback = null, List<QuestionAnswerData> choices = null)
    {
        Refresh();

        dialogText.SetText(text);
        _choices = choices;

        if (choices != null)
        {
            for (int i = 0; i < choices.Count; i++)
            {
                var choice = Instantiate(choicePrefab, choiceTransform);
                var id = _choices[i].id;
                choice.gameObject.GetComponentInChildren<TextMeshProUGUI>().SetText(_choices[i].question);
                choice.onClick.AddListener(()=> {
                    onClickCallback(id); 
                });

                if (_lastChoices != null && !_lastChoices.Contains(_choices[i]))
                {
                    choice.gameObject.GetComponent<Image>().color = newColor;
                }
            }

            var noItemChoice = Instantiate(choicePrefab, choiceTransform);
            noItemChoice.gameObject.GetComponentInChildren<TextMeshProUGUI>().SetText("Sorry we don't have this item");
            noItemChoice.onClick.AddListener(() => {
                _lastChoices.Clear();
                _lastChoices.TrimExcess();
                this.gameObject.SetActive(false);
                onNoItemFound(false);
            });
            noItemChoice.gameObject.GetComponent<Image>().color = noItemColor;

            choiceTransform.gameObject.SetActive(true);
            _lastChoices.AddRange(_choices);
        } else
        {
            choiceTransform.gameObject.SetActive(false);
            nextButton.gameObject.SetActive(true);
            nextButton.onClick.AddListener(() =>
            {
                onClickCallback("-1");
            });
        }

        
        Canvas.ForceUpdateCanvases();
        LayoutRebuilder.ForceRebuildLayoutImmediate(this.gameObject.GetComponent<RectTransform>());
        
    }

    public void Refresh()
    {
        nextButton.onClick.RemoveAllListeners();
        nextButton.gameObject.SetActive(false);
        foreach(Transform child in choiceTransform)
        {
            GameObject.Destroy(child.gameObject);
        }
        
    }
}
