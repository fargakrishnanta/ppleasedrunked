using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
using UnityEditorInternal;
#endif

[CustomEditor(typeof(DialogSetData))]
public class DialogSetEditor : Editor
{

    SerializedProperty introduction;
    SerializedProperty dialogsProp;
    ReorderableList dialogs;

    public void OnEnable()
    {
        introduction = serializedObject.FindProperty("introduction");
        dialogsProp = serializedObject.FindProperty("dialogs");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        EditorGUILayout.PropertyField(introduction);
        EditorGUILayout.PropertyField(dialogsProp);
        EditorGUILayout.PropertyField(serializedObject.FindProperty("correctResponse"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("wrongResponse"));
        serializedObject.ApplyModifiedProperties();
    }
}
