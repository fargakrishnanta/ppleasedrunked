using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class QuestionAnswerData
{
    public string id;
    public bool isUnlockedByDefault;
    public string question;
    public string answer;
    public List<string> unlockableQuestionsID = new List<string>();
}