using System.Collections;
using System.Collections.Generic;

using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

[CreateAssetMenu(menuName = "Drunk Assets/Dialog Set Data")]
public class DialogSetData : ScriptableObject
{
    public string introduction;
    public List<QuestionAnswerData> dialogs = new List<QuestionAnswerData>();
    public string correctResponse;
    public string wrongResponse;
}
