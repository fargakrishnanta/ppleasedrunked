using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ItemType { Wallet, Jacket, Phone } 

[CreateAssetMenu(menuName = "Drunk Assets/Item Data")]
public class ItemData : ScriptableObject
{
    public string ID;
    public ItemType itemType;
    public string location;
    public UDateTime dateFound;
}
