using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Drunk Assets/Character Data")]
public class CharacterData : ScriptableObject
{
    public string name;
    public DialogSetData dialog;
    public ItemData itemLost;
}
