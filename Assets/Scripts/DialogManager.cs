using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogManager : MonoBehaviour
{
    [SerializeField]
    private DialogSetData _currentDialogSet;

    public DialogController dialogController;

    bool isIntroPlayed = false;
    string currentDialogID = "";

    List<QuestionAnswerData> availableQAData = new List<QuestionAnswerData>();
    private string[] sequence;
    int actualIndex;

    public Action inDialogSetCompleted;

    public GameObject currentCharacter;
    public void Initiate(DialogSetData dialogSet)
    {
        dialogController.gameObject.SetActive(true);
        dialogController.onNoItemFound = ForceShowResult;
        Reset();
        _currentDialogSet = dialogSet;
        var unlockedDialogs = _currentDialogSet.dialogs.FindAll((item) => item.isUnlockedByDefault);
        availableQAData.AddRange(unlockedDialogs);
        Intro();
        
    }

    public void Intro()
    {
        sequence = _currentDialogSet.introduction.Split(new string[] { "===" }, System.StringSplitOptions.RemoveEmptyEntries);
        SetupSequence("0");
        isIntroPlayed = true;
        StartCoroutine(DoTalkingAnimation());
    }

    private void SetupSequence(string introIndex)
    {
        StartCoroutine(DoTalkingAnimation());
        actualIndex = Int32.Parse(introIndex);

        if (actualIndex < sequence.Length-1)
        {
            dialogController.Init(sequence[actualIndex], (index) => SetupSequence((++actualIndex).ToString()));
        } else
        {
            dialogController.Init(sequence[actualIndex], (index) => ShowAvailableChoices());
        }
    }


    private void SetupAnswer(string id)
    {
        var newUnlockedQASet = _currentDialogSet.dialogs.Find((item) => item.id == id).unlockableQuestionsID;
        availableQAData.RemoveAll((item) => item.id == id);

        foreach(var itemIndex in newUnlockedQASet)
        {
            if(availableQAData.FindAll(item => item.id == itemIndex).Count == 0)
            {
                availableQAData.Add(_currentDialogSet.dialogs.Find((item) => item.id == itemIndex));
            }
        }

        var answer = _currentDialogSet.dialogs.Find(item => item.id == id);
        sequence = answer.answer.Split(new string[] { "===" }, System.StringSplitOptions.RemoveEmptyEntries);

        actualIndex = 0;
        SetupSequence("0");
    }

    public void ShowAvailableChoices()
    {
        StopTalkingAnimation();
        dialogController.Init(sequence[actualIndex], (index) => SetupAnswer(index), availableQAData);
        actualIndex = 0;
    }

    public void Reset()
    {
        availableQAData = new List<QuestionAnswerData>();
    }

    public void ForceShowResult(bool isCorrect)
    {
        dialogController.gameObject.SetActive(true);

        var result = isCorrect ? _currentDialogSet.correctResponse : _currentDialogSet.wrongResponse;
        dialogController.Init(result, (index) => {
            inDialogSetCompleted();
            ToggleDialogVisibility(false);
            StopTalkingAnimation();
            });
    }

    public void ToggleDialogVisibility(bool isVisible)
    {
        dialogController.gameObject.SetActive(isVisible);
    }

    IEnumerator DoTalkingAnimation()
    {
        currentCharacter.GetComponent<CharacterAnimator>().SetSpeakAnimation(CharacterAnimator.SpeakAnimation.Speaking);

        yield return new WaitForSeconds(3);

        currentCharacter.GetComponent<CharacterAnimator>().SetSpeakAnimation(CharacterAnimator.SpeakAnimation.NotSpeaking);
    }

    void StopTalkingAnimation()
    {
        StopCoroutine(DoTalkingAnimation());
        currentCharacter.GetComponent<CharacterAnimator>().SetSpeakAnimation(CharacterAnimator.SpeakAnimation.NotSpeaking);
    }
}
