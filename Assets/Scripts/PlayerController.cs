using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;

public class PlayerController : MonoBehaviour
{
    public Material selectedMaterial;
    public Material defaultMaterial;

    public GameObject[] returnableObjects;

    public CameraController cameraController;
    public GameController gameController;
    private GameObject highlightedObj;
    private GameObject selectedObj;
    private GameObject pickedUpObj;

    public ItemDetailPopupReturn itemDetailPopup;

    public Transform objParents;

    void Start()
    {
        returnableObjects = GameObject.FindGameObjectsWithTag("Returnable");
        cameraController.onCameraMoved = CheckIfReturnableObjectInFront;
        cameraController.onCameraStoppedMoving = () =>
        {
            highlightedObj = null;
        };
    }

    void CheckIfReturnableObjectInFront()
    {
        if (isAnyObjectInFront(returnableObjects))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit))
            {
                if (hit.collider.gameObject.CompareTag("Returnable"))
                {
                    if (highlightedObj != hit.collider.gameObject)
                    {
                        //hit.collider.gameObject.GetComponent<MeshRenderer>().material.shader = Shader.Find("Outlined/UltimateOutline");
                        highlightedObj = hit.collider.gameObject;
                    }
                } else
                {
                    highlightedObj = null;
                }
            } else
            {
                highlightedObj = null;
            }

            foreach (var item in returnableObjects)
            {
                if(item != null)
                {
                    if (highlightedObj != null)
                    {
                        //if (item != highlightedObj && item != selectedObj)
                        //    item.GetComponent<MeshRenderer>().material.shader = Shader.Find("Standard");
                    }
                    else
                    {
                        //if (item != selectedObj)
                        //    item.GetComponent<MeshRenderer>().material.shader = Shader.Find("Standard");
                    }
                }
            }
        }
    }

    void CheckIfCustomerInFront()
    {
        if (gameController.started && !gameController.onComputer)
        {
            foreach (var item in gameController.characters)
            {
                if (item != null)
                {
                    if (Vector3.Dot(Camera.main.transform.forward, item.transform.position) > 0)
                    {
                        gameController.dialogManager.ToggleDialogVisibility(false);

                        return;
                    }
                }
            }

            gameController.dialogManager.ToggleDialogVisibility(true);
        }
        else
        {
            gameController.dialogManager.ToggleDialogVisibility(false);
        }
       
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (!CheckIfOverUI() && pickedUpObj == null)
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit))
                {
                    if (hit.collider.gameObject.CompareTag("Returnable"))
                    {
                        selectedObj = hit.collider.gameObject;
                        var returnableCont = hit.collider.gameObject.GetComponent<ReturnableObjController>();
                        itemDetailPopup.gameObject.SetActive(true);
                        itemDetailPopup.Init(returnableCont.itemData.ID, () => selectedObj = null, () => OnPickUp());
                    }
                } 
            }
            else if (!CheckIfOverUI() && pickedUpObj != null)
            {
                pickedUpObj.GetComponent<Rigidbody>().AddForce(cameraController.gameObject.transform.forward * 5, ForceMode.Impulse);
                pickedUpObj.GetComponent<BoxCollider>().enabled = true;
                pickedUpObj.GetComponent<Rigidbody>().useGravity = true;
                gameController.CheckIfReturnedItemCorrect(pickedUpObj.GetComponent<ReturnableObjController>(), () => {
                    StartCoroutine(WaitToDestroy(() =>
                    {
                        pickedUpObj.transform.SetParent(null);
                        GameObject.Destroy(pickedUpObj);
                        pickedUpObj = null;
                    }));
                    
                });
               
            }
        }
        CheckIfCustomerInFront();
    }

    IEnumerator WaitToDestroy(Action afterWait)
    {
        yield return new WaitForSeconds(1);
        afterWait();
    }

    private bool CheckIfOverUI()
    {
        var eventData = new PointerEventData(EventSystem.current);
        eventData.position = Input.mousePosition;
        var results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventData, results);

        return results.Where(r => r.gameObject.layer == 5 ).Count() > 0; ;
    }

    bool isAnyObjectInFront(GameObject[] objs)
    {
        foreach (var item in objs)
        {
            if(item != null)
            {
                if (Vector3.Dot(cameraController.transform.forward, item.transform.position) > 0)
                {
                    return true;
                }
            }
        }
        return false;
    }

    void OnPickUp()
    {
        pickedUpObj = selectedObj;
        selectedObj = null;
        pickedUpObj.transform.position = cameraController.holdingTransform.position;
        pickedUpObj.transform.SetParent(cameraController.holdingTransform);
        pickedUpObj.GetComponent<BoxCollider>().enabled = false;
        pickedUpObj.GetComponent<Rigidbody>().useGravity = false;
        itemDetailPopup.gameObject.SetActive(false);
    }
}
