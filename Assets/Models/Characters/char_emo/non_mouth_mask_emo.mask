%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: non_mouth_mask_emo
  m_Mask: 00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: body
    m_Weight: 0
  - m_Path: char.armature
    m_Weight: 0
  - m_Path: char.armature/body.bottom
    m_Weight: 1
  - m_Path: char.armature/body.bottom/body.top
    m_Weight: 1
  - m_Path: char.armature/body.bottom/body.top/head 1
    m_Weight: 1
  - m_Path: char.armature/body.bottom/body.top/head 1/def.mouth.bottom
    m_Weight: 0
  - m_Path: char.armature/body.bottom/body.top/head 1/def.mouth.bottom/def.mouth.bottom_end
    m_Weight: 0
  - m_Path: char.armature/body.bottom/body.top/head 1/def.mouth.top
    m_Weight: 0
  - m_Path: char.armature/body.bottom/body.top/head 1/def.mouth.top/def.mouth.top_end
    m_Weight: 0
  - m_Path: char.armature/body.bottom/body.top/head 1/def.tongue.1
    m_Weight: 0
  - m_Path: char.armature/body.bottom/body.top/head 1/def.tongue.1/def.tongue.2
    m_Weight: 0
  - m_Path: char.armature/body.bottom/body.top/head 1/def.tongue.1/def.tongue.2/def.tongue.3
    m_Weight: 0
  - m_Path: char.armature/body.bottom/body.top/head 1/def.tongue.1/def.tongue.2/def.tongue.3/def.tongue.4
    m_Weight: 0
  - m_Path: char.armature/body.bottom/body.top/head 1/def.tongue.1/def.tongue.2/def.tongue.3/def.tongue.4/def.tongue.4_end
    m_Weight: 0
  - m_Path: char.armature/body.bottom/body.top/head 1/eye.bottom.L.001
    m_Weight: 1
  - m_Path: char.armature/body.bottom/body.top/head 1/eye.bottom.L.001/eye.bottom.L.001_end
    m_Weight: 0
  - m_Path: char.armature/body.bottom/body.top/head 1/eye.bottom.L.002
    m_Weight: 1
  - m_Path: char.armature/body.bottom/body.top/head 1/eye.bottom.L.002/eye.bottom.L.002_end
    m_Weight: 0
  - m_Path: char.armature/body.bottom/body.top/head 1/eye.bottom.L.003
    m_Weight: 1
  - m_Path: char.armature/body.bottom/body.top/head 1/eye.bottom.L.003/eye.bottom.L.003_end
    m_Weight: 0
  - m_Path: char.armature/body.bottom/body.top/head 1/eye.bottom.L.004
    m_Weight: 1
  - m_Path: char.armature/body.bottom/body.top/head 1/eye.bottom.L.004/eye.bottom.L.004_end
    m_Weight: 0
  - m_Path: char.armature/body.bottom/body.top/head 1/eye.bottom.L.005
    m_Weight: 1
  - m_Path: char.armature/body.bottom/body.top/head 1/eye.bottom.L.005/eye.bottom.L.005_end
    m_Weight: 0
  - m_Path: char.armature/body.bottom/body.top/head 1/eye.bottom.R.001
    m_Weight: 1
  - m_Path: char.armature/body.bottom/body.top/head 1/eye.bottom.R.001/eye.bottom.R.001_end
    m_Weight: 0
  - m_Path: char.armature/body.bottom/body.top/head 1/eye.bottom.R.002
    m_Weight: 1
  - m_Path: char.armature/body.bottom/body.top/head 1/eye.bottom.R.002/eye.bottom.R.002_end
    m_Weight: 0
  - m_Path: char.armature/body.bottom/body.top/head 1/eye.bottom.R.003
    m_Weight: 1
  - m_Path: char.armature/body.bottom/body.top/head 1/eye.bottom.R.003/eye.bottom.R.003_end
    m_Weight: 0
  - m_Path: char.armature/body.bottom/body.top/head 1/eye.bottom.R.004
    m_Weight: 1
  - m_Path: char.armature/body.bottom/body.top/head 1/eye.bottom.R.004/eye.bottom.R.004_end
    m_Weight: 0
  - m_Path: char.armature/body.bottom/body.top/head 1/eye.bottom.R.005
    m_Weight: 1
  - m_Path: char.armature/body.bottom/body.top/head 1/eye.bottom.R.005/eye.bottom.R.005_end
    m_Weight: 0
  - m_Path: char.armature/body.bottom/body.top/head 1/eye.left_middle.L
    m_Weight: 1
  - m_Path: char.armature/body.bottom/body.top/head 1/eye.left_middle.L/eye.left_middle.L_end
    m_Weight: 0
  - m_Path: char.armature/body.bottom/body.top/head 1/eye.left_middle.R
    m_Weight: 1
  - m_Path: char.armature/body.bottom/body.top/head 1/eye.left_middle.R/eye.left_middle.R_end
    m_Weight: 0
  - m_Path: char.armature/body.bottom/body.top/head 1/eye.right_middle.L
    m_Weight: 1
  - m_Path: char.armature/body.bottom/body.top/head 1/eye.right_middle.L/eye.right_middle.L_end
    m_Weight: 0
  - m_Path: char.armature/body.bottom/body.top/head 1/eye.right_middle.R
    m_Weight: 1
  - m_Path: char.armature/body.bottom/body.top/head 1/eye.right_middle.R/eye.right_middle.R_end
    m_Weight: 0
  - m_Path: char.armature/body.bottom/body.top/head 1/eye.top.L.001
    m_Weight: 1
  - m_Path: char.armature/body.bottom/body.top/head 1/eye.top.L.001/eye.top.L.001_end
    m_Weight: 0
  - m_Path: char.armature/body.bottom/body.top/head 1/eye.top.L.002
    m_Weight: 1
  - m_Path: char.armature/body.bottom/body.top/head 1/eye.top.L.002/eye.top.L.002_end
    m_Weight: 0
  - m_Path: char.armature/body.bottom/body.top/head 1/eye.top.L.003
    m_Weight: 1
  - m_Path: char.armature/body.bottom/body.top/head 1/eye.top.L.003/eye.top.L.003_end
    m_Weight: 0
  - m_Path: char.armature/body.bottom/body.top/head 1/eye.top.L.004
    m_Weight: 1
  - m_Path: char.armature/body.bottom/body.top/head 1/eye.top.L.004/eye.top.L.004_end
    m_Weight: 0
  - m_Path: char.armature/body.bottom/body.top/head 1/eye.top.L.005
    m_Weight: 1
  - m_Path: char.armature/body.bottom/body.top/head 1/eye.top.L.005/eye.top.L.005_end
    m_Weight: 0
  - m_Path: char.armature/body.bottom/body.top/head 1/eye.top.R.001
    m_Weight: 1
  - m_Path: char.armature/body.bottom/body.top/head 1/eye.top.R.001/eye.top.R.001_end
    m_Weight: 0
  - m_Path: char.armature/body.bottom/body.top/head 1/eye.top.R.002
    m_Weight: 1
  - m_Path: char.armature/body.bottom/body.top/head 1/eye.top.R.002/eye.top.R.002_end
    m_Weight: 0
  - m_Path: char.armature/body.bottom/body.top/head 1/eye.top.R.003
    m_Weight: 1
  - m_Path: char.armature/body.bottom/body.top/head 1/eye.top.R.003/eye.top.R.003_end
    m_Weight: 0
  - m_Path: char.armature/body.bottom/body.top/head 1/eye.top.R.004
    m_Weight: 1
  - m_Path: char.armature/body.bottom/body.top/head 1/eye.top.R.004/eye.top.R.004_end
    m_Weight: 0
  - m_Path: char.armature/body.bottom/body.top/head 1/eye.top.R.005
    m_Weight: 1
  - m_Path: char.armature/body.bottom/body.top/head 1/eye.top.R.005/eye.top.R.005_end
    m_Weight: 0
  - m_Path: char.armature/body.bottom/body.top/head 1/eyebrow.L.001
    m_Weight: 1
  - m_Path: char.armature/body.bottom/body.top/head 1/eyebrow.L.001/eyebrow.L.002
    m_Weight: 0
  - m_Path: char.armature/body.bottom/body.top/head 1/eyebrow.L.001/eyebrow.L.002/eyebrow.L.003
    m_Weight: 0
  - m_Path: char.armature/body.bottom/body.top/head 1/eyebrow.L.001/eyebrow.L.002/eyebrow.L.003/eyebrow.L.004
    m_Weight: 0
  - m_Path: char.armature/body.bottom/body.top/head 1/eyebrow.L.001/eyebrow.L.002/eyebrow.L.003/eyebrow.L.004/eyebrow.L.004_end
    m_Weight: 0
  - m_Path: char.armature/body.bottom/body.top/head 1/eyebrow.R.001
    m_Weight: 1
  - m_Path: char.armature/body.bottom/body.top/head 1/eyebrow.R.001/eyebrow.R.002
    m_Weight: 0
  - m_Path: char.armature/body.bottom/body.top/head 1/eyebrow.R.001/eyebrow.R.002/eyebrow.R.003
    m_Weight: 0
  - m_Path: char.armature/body.bottom/body.top/head 1/eyebrow.R.001/eyebrow.R.002/eyebrow.R.003/eyebrow.R.004
    m_Weight: 0
  - m_Path: char.armature/body.bottom/body.top/head 1/eyebrow.R.001/eyebrow.R.002/eyebrow.R.003/eyebrow.R.004/eyebrow.R.004_end
    m_Weight: 0
  - m_Path: char.armature/body.bottom/body.top/head 1/mouth.bottom
    m_Weight: 0
  - m_Path: char.armature/body.bottom/body.top/head 1/mouth.bottom/mouth.bottom_end
    m_Weight: 0
  - m_Path: char.armature/body.bottom/body.top/head 1/mouth.bottom.L.001
    m_Weight: 0
  - m_Path: char.armature/body.bottom/body.top/head 1/mouth.bottom.L.001/mouth.bottom.L.001_end
    m_Weight: 0
  - m_Path: char.armature/body.bottom/body.top/head 1/mouth.bottom.L.002
    m_Weight: 0
  - m_Path: char.armature/body.bottom/body.top/head 1/mouth.bottom.L.002/mouth.bottom.L.002_end
    m_Weight: 0
  - m_Path: char.armature/body.bottom/body.top/head 1/mouth.bottom.L.003
    m_Weight: 0
  - m_Path: char.armature/body.bottom/body.top/head 1/mouth.bottom.L.003/mouth.bottom.L.003_end
    m_Weight: 0
  - m_Path: char.armature/body.bottom/body.top/head 1/mouth.bottom.R.001
    m_Weight: 0
  - m_Path: char.armature/body.bottom/body.top/head 1/mouth.bottom.R.001/mouth.bottom.R.001_end
    m_Weight: 0
  - m_Path: char.armature/body.bottom/body.top/head 1/mouth.bottom.R.002
    m_Weight: 0
  - m_Path: char.armature/body.bottom/body.top/head 1/mouth.bottom.R.002/mouth.bottom.R.002_end
    m_Weight: 0
  - m_Path: char.armature/body.bottom/body.top/head 1/mouth.bottom.R.003
    m_Weight: 0
  - m_Path: char.armature/body.bottom/body.top/head 1/mouth.bottom.R.003/mouth.bottom.R.003_end
    m_Weight: 0
  - m_Path: char.armature/body.bottom/body.top/head 1/mouth.top
    m_Weight: 0
  - m_Path: char.armature/body.bottom/body.top/head 1/mouth.top/mouth.top_end
    m_Weight: 0
  - m_Path: char.armature/body.bottom/body.top/head 1/mouth.top.L.001
    m_Weight: 0
  - m_Path: char.armature/body.bottom/body.top/head 1/mouth.top.L.001/mouth.top.L.001_end
    m_Weight: 0
  - m_Path: char.armature/body.bottom/body.top/head 1/mouth.top.L.002
    m_Weight: 0
  - m_Path: char.armature/body.bottom/body.top/head 1/mouth.top.L.002/mouth.top.L.002_end
    m_Weight: 0
  - m_Path: char.armature/body.bottom/body.top/head 1/mouth.top.L.003
    m_Weight: 0
  - m_Path: char.armature/body.bottom/body.top/head 1/mouth.top.L.003/mouth.top.L.003_end
    m_Weight: 0
  - m_Path: char.armature/body.bottom/body.top/head 1/mouth.top.R.001
    m_Weight: 0
  - m_Path: char.armature/body.bottom/body.top/head 1/mouth.top.R.001/mouth.top.R.001_end
    m_Weight: 0
  - m_Path: char.armature/body.bottom/body.top/head 1/mouth.top.R.002
    m_Weight: 0
  - m_Path: char.armature/body.bottom/body.top/head 1/mouth.top.R.002/mouth.top.R.002_end
    m_Weight: 0
  - m_Path: char.armature/body.bottom/body.top/head 1/mouth.top.R.003
    m_Weight: 0
  - m_Path: char.armature/body.bottom/body.top/head 1/mouth.top.R.003/mouth.top.R.003_end
    m_Weight: 0
  - m_Path: eye.lashes
    m_Weight: 0
  - m_Path: eyebrow
    m_Weight: 0
  - m_Path: eyes
    m_Weight: 0
  - m_Path: eyes/pupil
    m_Weight: 0
  - m_Path: hair.left
    m_Weight: 0
  - m_Path: hair.right
    m_Weight: 0
  - m_Path: hair.top
    m_Weight: 0
  - m_Path: hait.front
    m_Weight: 0
  - m_Path: head
    m_Weight: 0
  - m_Path: mouth
    m_Weight: 0
  - m_Path: tongue
    m_Weight: 0
